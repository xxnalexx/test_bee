<?php

return [
    'pagination' => [
      'size' => envVar('APP_PER_PAGE', 3)
    ],
    'doctrine' => [
        // if true, metadata caching is forcefully disabled
        'dev_mode' => true,

        // path where the compiled metadata info will be cached
        // make sure the path exists and it is writable
        'cache_dir' => APP_ROOT . '/cache/doctrine',

        // you should add any other path containing annotated entity classes
        'metadata_dirs' => [APP_ROOT . '/app/Entity'],

        'connection' => [
            'driver' => envVar('DB_DRIVER', 'pdo_mysql'),
            'host' => envVar('DB_HOST', 'localhost'),
            'port' => envVar('DB_PORT', 3306),
            'dbname' => envVar('DB_NAME', 'database'),
            'user' => envVar('DB_USER', 'root'),
            'password' => envVar('DB_PASSWORD', 'root'),
            'charset' => 'utf8mb4'
        ]
    ]
];
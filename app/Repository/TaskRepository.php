<?php

namespace App\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

class TaskRepository extends \Doctrine\ORM\EntityRepository
{
    const STATUS_PROGRESS = 1;
    const STATUS_DONE = 2;

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
          self::STATUS_PROGRESS => 'In progress',
          self::STATUS_DONE => 'Done',
        ];
    }

    /**
     * @return array
     */
    public function getCrudColumns()
    {
        return [
            'name' => [
                'title' => 'Name',
                'sortable' => true,
                'class' => 'col-3'
            ],
            'email' => [
                'title' => 'E-mail',
                'sortable' => true,
                'class' => 'col-3'
            ],
            'text' => [
                'title' => 'Text',
                'sortable' => false,
                'class' => 'col-3'
            ],
            'status' => [
                'title' => 'Status',
                'sortable' => true,
                'class' => 'col-2'
            ],
        ];
    }

    /**
     * @param $status
     * @return mixed|string
     */
    public static function getStatusText($status)
    {
        $statuses = self::getStatuses();

        return isset($statuses[$status]) ? $statuses[$status] : '';
    }

    /**
     * @param int $perPage
     * @return Paginator
     */
    public function getQuery(?array $criteria, ?array $order)
    {
        $queryBuilder = $this->createQueryBuilder('a');

        if (!empty($order)) {
            $queryBuilder->orderBy('a.' . $order['column'], $order['dir']);
        }

        return new Paginator($queryBuilder);
    }

}
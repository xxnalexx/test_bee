<?php

if( !session_id() ) @session_start();

define('APP_ROOT', dirname(__DIR__));

require_once __DIR__ . '/helpers.php';
require_once __DIR__.'/../vendor/autoload.php';

(new \App\Kernel\LoadEnv(
    dirname(__DIR__)
))->load();

$app = new App\Kernel\Application(
    dirname(__DIR__)
);

/* Home routes */
$app->router->get('/', '\App\Controllers\HomeController:index');
$app->router->post('/tasks/store', '\App\Controllers\HomeController:storeTask');

/* Auth routes */
$app->router->get('/login', '\App\Controllers\LoginController:showLoginForm');
$app->router->post('/login', '\App\Controllers\LoginController:login');
$app->router->post('/logout', '\App\Controllers\LoginController:logout');

/* Admin routes */
$app->router->get('/tasks/{id}', '\App\Controllers\AdminController:editTask', ['middleware' => \App\Middleware\AuthMiddleware::class]);
$app->router->post('/tasks/{id}', '\App\Controllers\AdminController:updateTask', ['middleware' => \App\Middleware\AuthMiddleware::class]);

return $app;
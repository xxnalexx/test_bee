<?php

namespace App\Kernel;

use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NotFoundHandler
{
    /**
     * @var Container
     */
    private $container;

    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request)
    {
        $viewInstance = $this->container->offsetGet('view');

        return new Response($viewInstance->view()->make('errors.404')->render(), Response::HTTP_NOT_FOUND);
    }
}
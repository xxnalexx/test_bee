<?php

namespace App\Kernel;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class RequestResponse
{
    /**
     * Invoke a route callable with request, and all route parameters
     * as an array of arguments.
     *
     */
    public function __invoke(
        callable $callable,
        SymfonyRequest $request,
        array $routeArguments
    ) {
        return call_user_func($callable, $request, $routeArguments);
    }
}
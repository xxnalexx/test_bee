<?php

namespace App\Kernel;

use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use FastRoute\RouteParser;
use FastRoute\RouteParser\Std as StdParser;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class Router
{

    /**
     * Container
     *
     */
    protected $container;

    /**
     * Routes
     *
     * @var \App\Kernel\Route[]
     */
    protected $routes = [];

    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * Parser
     *
     * @var RouteParser
     */
    protected $routeParser;

    /**
     * Route counter incrementer
     * @var int
     */
    protected $routeCounter = 0;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->routeParser = new StdParser;
    }

    /**
     * @param $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @param SymfonyRequest $request
     * @return array
     */
    public function dispatch(SymfonyRequest $request)
    {
        $uri = '/' . ltrim($request->getPathInfo(), '/');

        return $this->createDispatcher()->dispatch(
            $request->getMethod(),
            $uri
        );
    }

    /**
     * @return Dispatcher
     */
    protected function createDispatcher()
    {
        if ($this->dispatcher) {
            return $this->dispatcher;
        }

        $routeDefinitionCallback = function (RouteCollector $r) {
            foreach ($this->getRoutes() as $route) {
                $r->addRoute($route->getMethods(), $route->getPattern(), $route->getIdentifier());
            }
        };

        $this->dispatcher = \FastRoute\simpleDispatcher($routeDefinitionCallback, [
            'routeParser' => $this->routeParser,
        ]);

        return $this->dispatcher;
    }

    /**
     * Get route objects
     *
     * @return Route[]
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param $methods
     * @param $pattern
     * @param $handler
     * @return \App\Kernel\Route
     */
    public function map($methods, $pattern, $handler, $settings)
    {
        if (!is_string($pattern)) {
            throw new \RuntimeException('Route pattern must be a string');
        }

        $methods = array_map("strtoupper", $methods);

        /** @var \App\Kernel\Route $route */
        $route = $this->createRoute($methods, $pattern, $handler, $settings);

        $this->routes[$route->getIdentifier()] = $route;
        $this->routeCounter++;

        return $route;
    }

    /**
     * Create a new Route object
     *
     * @param  string[] $methods Array of HTTP methods
     * @param  string   $pattern The route pattern
     * @param  callable $callable The route callable
     *
     * @return \App\Kernel\Route;
     */
    protected function createRoute($methods, $pattern, $callable, $settings)
    {
        $route = new \App\Kernel\Route($methods, $pattern, $callable, $this->routeCounter, $settings);

        if (!empty($this->container)) {
            $route->setContainer($this->container);
        }

        return $route;
    }

    /**
     * Add GET route
     *
     * @param  string          $pattern  The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return
     */
    public function get($pattern, $callable, $settings = [])
    {
        return $this->map(['GET'], $pattern, $callable, $settings);
    }

    /**
     * Add GET route
     *
     * @param  string          $pattern  The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return
     */
    public function post($pattern, $callable, $settings = [])
    {
        return $this->map(['POST'], $pattern, $callable, $settings);
    }

    /**
     * @param string $identifier
     * @return \App\Kernel\Route
     */
    public function getRoute(string $identifier)
    {
        if (!isset($this->routes[$identifier])) {
            throw new RuntimeException('Route not found, looks like your route cache is stale.');
        }

        return $this->routes[$identifier];
    }

}

<?php

namespace App\Kernel;

use App\Entity\User;
use App\Exceptions\AuthException;
use Doctrine\ORM\EntityManager;

class Auth
{
    /**
     * @param EntityManager $em
     * @param array $credentials
     * @throws AuthException
     */
    public static function authorize(EntityManager $em, array $credentials)
    {
        $userRepository = $em->getRepository(User::class);

        /**
         * @var User
         */
        $user = $userRepository->findOneBy(['login' => $credentials['login']]);

        if (empty($user) || !password_verify($credentials['password'], $user->getPassword())) {
            throw new AuthException("User not found");
        }

        $_SESSION['user']['id'] = $user->getId();
    }

    /**
     * Verify if user is authorized in system
     *
     * @return bool
     */
    public static function check()
    {
        return isset($_SESSION['user']);
    }

    /**
     * Unset user authorization
     *
     */
    public static function logout()
    {
        if (self::check()) {
            unset($_SESSION['user']);
        }
    }
}
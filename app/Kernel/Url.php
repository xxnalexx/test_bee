<?php

namespace App\Kernel;

use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;

class Url
{
    /**
     * @var Container
     */
    public $container;

    /**
     * Url constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @param string|null $queryParams
     * @return string
     */
    public function fullUrl(?string $queryParams)
    {
        /**
         * @var Request $request
         */
        $request = $this->container->offsetGet('request');

        return $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . $queryParams;
    }

    /**
     * @param array $params
     * @return string
     */
    public function prepareQueryParams(array $params)
    {
        $preparedParams = [];

        foreach ($params as $k => $v) {
            $preparedParams[] = $k .'=' . $v;
        }

        return count($preparedParams) > 0 ? '?' . implode("&", $preparedParams) : '';
    }

    /**
     * @param string $param
     * @param null $value
     * @return array
     */
    public function replaceQueryParams(string $param, $value = null)
    {
        /**
         * @var Request $request
         */
        $request = $this->container->offsetGet('request');
        $params = $request->query->all();

        if (!empty($value)) {
            $params[$param] = $value;
        }

        return $params;
    }
}
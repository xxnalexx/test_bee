<?php

namespace App\Kernel;

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidFileException;
use Dotenv\Repository\Adapter\EnvConstAdapter;
use Dotenv\Repository\Adapter\PutenvAdapter;
use Dotenv\Repository\Adapter\ServerConstAdapter;
use Dotenv\Repository\RepositoryBuilder;
use PhpOption\Option;

class LoadEnv
{

    /**
     * @var string
     */
    protected $filePath;

    /**
     * Create a new loads environment variables instance.
     *
     * @param  string  $path
     * @return void
     */
    public function __construct($path)
    {
        $this->filePath = $path;
    }

    /**
     * Load dotenv configuration
     */
    public function load()
    {
        try {
            $this->createDotenv()->safeLoad();
        } catch (InvalidFileException $e) {
            echo 'Error load env';
            die(1);
        }
    }


    /**
     * Create a Dotenv instance.
     *
     * @return \Dotenv\Dotenv
     */
    protected function createDotenv()
    {
        return Dotenv::create(
            $this->getEnvRepository(),
            $this->filePath
        );
    }

    /**
     * @return \Dotenv\Repository\RepositoryInterface
     */
    protected function getEnvRepository()
    {
        $adapters = [new EnvConstAdapter, new ServerConstAdapter];

        return RepositoryBuilder::create()
            ->withReaders($adapters)
            ->withWriters($adapters)
            ->immutable()
            ->make();
    }
}
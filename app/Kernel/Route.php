<?php

namespace App\Kernel;

use Closure;
use Pimple\Container;
use RuntimeException;

class Route
{

    /**
     * Container
     * @var Container
     */
    protected $container;

    /**
     * Route pattern
     *
     * @var string
     */
    protected $pattern;


    /**
     * Route callable
     *
     * @var callable
     */
    protected $callable;


    /**
     * HTTP methods supported by this route
     *
     * @var string[]
     */
    protected $methods = [];

    /**
     * Route identifier
     *
     * @var string
     */
    protected $identifier;

    /**
     * Route parameters
     *
     * @var array
     */
    protected $arguments = [];

    /**
     * Route arguments parameters
     *
     * @var null|array
     */
    protected $savedArguments = [];

    /**
     * @var array
     */
    protected $routeSettings = [];

    /**
     * @param string|string[] $methods The route HTTP methods
     * @param string          $pattern The route pattern
     * @param callable        $callable The route callable
     * @param int             $identifier The route identifier
     */
    public function __construct($methods, $pattern, $callable, $identifier = 0, $settings = [])
    {
        $this->pattern = $pattern;
        $this->callable = $callable;
        $this->methods  = is_string($methods) ? [$methods] : $methods;
        $this->identifier = 'route' . $identifier;
        $this->settings = $settings;
    }

    /**
     * Set container for use with resolveCallable
     *
     * @param  $container
     *
     * @return static
     */
    public function setContainer( $container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * Get route methods
     *
     * @return string[]
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * Get route identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Get the route pattern
     *
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @param $name
     * @param $value
     * @param bool $includeInSavedArguments
     * @return $this
     */
    public function setArgument($name, $value, $includeInSavedArguments = true)
    {
        if ($includeInSavedArguments) {
            $this->savedArguments[$name] = $value;
        }
        $this->arguments[$name] = $value;
        return $this;
    }

    /**
     * @param array $arguments
     * @param bool $includeInSavedArguments
     * @return $this
     */
    public function setArguments(array $arguments, $includeInSavedArguments = true)
    {
        if ($includeInSavedArguments) {
            $this->savedArguments = $arguments;
        }
        $this->arguments = $arguments;
        return $this;
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param $name
     * @param null $default
     * @return mixed|null
     */
    public function getArgument($name, $default = null)
    {
        if (array_key_exists($name, $this->arguments)) {
            return $this->arguments[$name];
        }
        return $default;
    }

    /**
     * @param $request
     * @param array $arguments
     */
    public function prepare($request, array $arguments)
    {
        // Remove temp arguments
        $this->setArguments($this->savedArguments);

        // Add the route arguments
        foreach ($arguments as $k => $v) {
            $this->setArgument($k, $v, false);
        }
    }

    /**
     * Resolve a string of the format 'class:method' into a closure that the
     * router can dispatch.
     *
     * @param callable|string $callable
     *
     * @return Closure
     *
     * @throws RuntimeException If the string cannot be resolved as a callable
     */
    protected function resolveCallable($callable)
    {
        $resolver = $this->container->offsetGet('callableResolver');

        return $resolver->resolve($callable);
    }

    /**
     * Run route call
     *
     * @return mixed
     */
    public function run($request)
    {
        $this->callable = $this->resolveCallable($this->callable);

        $handler = isset($this->container) ? $this->container->offsetGet('foundHandler') : new RequestResponse();

        $newResponse = $handler($this->callable, $request, $this->arguments);

        return $newResponse;
    }

    /**
     * @return bool|mixed
     */
    public function runMiddleware()
    {
        if (is_array($this->settings) && isset($this->settings['middleware'])) {
            return $this->container->offsetGet($this->settings['middleware'])->handle();
        }

        return true;
    }
}

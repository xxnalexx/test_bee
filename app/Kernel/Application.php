<?php

namespace App\Kernel;

use App\Kernel\Router;
use App\Providers\RepositoryServiceProvider;
use FastRoute\Dispatcher;
use Psr\Container\ContainerInterface;
use Pimple\Container as PimpleContainer;
use App\Providers\DefaultServicesProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class Application
{
    /**
     * The base path of the application installation.
     *
     * @var string
     */
    protected $basePath;

    /**
     * The Router instance.
     *
     * @var \App\Kernel\Router
     */
    public $router;

    /**
     * @var PimpleContainer
     */
    private $container;


    /**
     * @param SymfonyRequest $request
     * @return mixed
     */
    protected function dispatchRouterAndPrepareRoute(SymfonyRequest $request)
    {
        $routeInfo = $this->router->dispatch($request);

        if ($routeInfo[0] === Dispatcher::FOUND) {
            $routeArguments = [];
            foreach ($routeInfo[2] as $k => $v) {
                $routeArguments[$k] = urldecode($v);
            }

            $route = $this->router->getRoute($routeInfo[1]);

            $response = $route->runMiddleware();

            if (!is_bool($response)) {
                return $response;
            }

            $route->prepare($request, $routeArguments);

            return $route->run($request);

        }

        /** @var callable $notFoundHandler */
        $notFoundHandler = $this->container->offsetGet('notFoundHandler');
        return $notFoundHandler($request);
    }

    /**
     *
     * @param  string|null  $basePath
     * @return void
     */
    public function __construct($basePath = null)
    {
        $this->basePath = $basePath;

        $this->bootstrapContainer();
        $this->bootstrapServiceProviders();
        $this->bootstrapRouter();
    }

    /**
     *
     */
    public function bootstrapContainer()
    {
        $config['app'] = require APP_ROOT . '/config/app.php';

        $container = new PimpleContainer($config);

        $this->container = $container;
    }

    /**
     * Bootstrap the router instance.
     *
     * @return void
     */
    public function bootstrapRouter()
    {
        $this->router = $this->container->offsetGet('router');
    }

    /**
     *
     */
    public function bootstrapServiceProviders()
    {
        $providers = [
            DefaultServicesProvider::class
        ];

        foreach ($providers as $provider) {
            $providerInstance = new $provider;
            $providerInstance->register($this->container);
        }
    }

    /**
     * @return PimpleContainer
     */
    public function getContainer()
    {
        return $this->container;
    }


    /**
     * Start application
     */
    public function run()
    {
        /**
         * @var Request $request
         */
        $request = $this->container->offsetGet('request');

        $response = $this->dispatchRouterAndPrepareRoute($request);

        if ($response instanceof SymfonyResponse) {
            $response->send();
        } else {
            echo (string) $response;
        }

    }
}
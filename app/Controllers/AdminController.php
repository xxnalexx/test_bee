<?php

namespace App\Controllers;

use App\Entity\Task;
use App\Middleware\AuthMiddleware;
use Pimple\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Tamtamchik\SimpleFlash\Flash;

class AdminController extends Controller
{
    /**
     * AdminController constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);

        return $this->container->offsetGet(AuthMiddleware::class)->handle();
    }

    /**
     * @param Request $request
     * @param array $args
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function editTask(Request $request, array $args)
    {
        $taskRepository = $this->em->getRepository(Task::class);
        $task = $taskRepository->find($args['id']);

        if (empty($task)) {
            return $this->abort(404);
        }

        return $this->view('tasks.edit', [
            'task' => $task
        ]);
    }

    /**
     * @param Request $request
     * @param array $args
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateTask(Request $request, array $args)
    {
        $taskRepository = $this->em->getRepository(Task::class);
        $task = $taskRepository->find($args['id']);

        if (empty($task)) {
            $this->abort(404);
        }

        $data = $request->request->all();
        $validator = $this->container->offsetGet('validator');

        $validation = $validator->validate($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'text' => 'required',
            'status' => 'required'
        ]);

        if ($validation->fails()) {
            $errors = $validation->errors();
            Flash::error($errors->firstOfAll());
            return new RedirectResponse('/tasks/' . $args['id']);
        }

        $task->setName($data['name']);
        $task->setEmail($data['email']);
        $task->setStatus($data['status']);
        $task->setText($data['text']);

        $this->em->persist($task);
        $this->em->flush();

        Flash::success("Task updated");

        return new RedirectResponse($request->headers->get('referer'));
    }
}
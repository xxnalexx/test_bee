<?php

namespace App\Controllers;

use Doctrine\ORM\EntityManager;
use Pimple\Container;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class Controller
{
    /**
     * @var Container
     */
    public $container;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * Controller constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
       $this->container = $container;
       $this->em = $this->container->offsetGet(EntityManager::class);
    }

    /**
     * @param $template
     * @param array $args
     * @return mixed
     */
    public function view($template, $args = [])
    {
        $viewInstance = $this->container->offsetGet('view');

        return $viewInstance->view()->make($template, $args)->render();
    }

    /**
     * @param int $code
     * @return SymfonyResponse
     */
    public function abort($code = 404)
    {
        return new SymfonyResponse($this->view('errors.404'), $code);
    }
}
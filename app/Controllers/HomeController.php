<?php

namespace App\Controllers;

use App\Entity\Task;
use App\Helpers\Pagination;
use App\Helpers\SortTable;
use Symfony\Component\HttpFoundation\Request;
use \Tamtamchik\SimpleFlash\Flash;
use Symfony\Component\HttpFoundation\RedirectResponse;

class HomeController extends Controller
{

    /**
     * @param Request $request
     * @param array $args
     * @return mixed
     */
    public function index(Request $request)
    {
        $pageSize = $this->container->offsetGet('app')['pagination']['size'];
        $paginator = $this->container->offsetGet(Pagination::class);
        $sortTable = $this->container->offsetGet(SortTable::class);

        $taskRepository = $this->em->getRepository(Task::class);
        $columns = $taskRepository->getCrudColumns();
        $sortTable->prepareSort($request->query->get('sort', ""), array_keys($columns));

        $pagination = $paginator->paginate(
            $taskRepository->getQuery([], $sortTable->getSortParams()),
            $request->query->getInt('page', 1),
            $pageSize
        );

        return $this->view('home', [
            'tasks' => $pagination,
            'columns' => $columns,
            'sortTable' => $sortTable
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function storeTask(Request $request)
    {
        $data = $request->request->all();
        $validator = $this->container->offsetGet('validator');

        $validation = $validator->validate($data, [
            'name'                  => 'required|max:255',
            'email'                 => 'required|email|max:255',
            'text'                 => 'required',
        ]);

        if ($validation->fails()) {

            $errors = $validation->errors();
            Flash::error($errors->firstOfAll());
            return new RedirectResponse('/');
        }

        $task = new Task();
        $task->setEmail($data['email']);
        $task->setName($data['name']);
        $task->setStatus(1);
        $task->setText($data['text']);

        $this->em->persist($task);
        $this->em->flush();

        Flash::success("Task created");

        return new RedirectResponse('/');
    }
}
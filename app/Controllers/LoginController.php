<?php

namespace App\Controllers;

use App\Exceptions\AuthException;
use App\Kernel\Auth;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Tamtamchik\SimpleFlash\Flash;

class LoginController extends Controller
{
    /**
     * @return mixed
     */
    public function showLoginForm()
    {
        return $this->view('auth.login');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function login(Request $request)
    {
        try {

            $data = $request->request->all();
            $validator = $this->container->offsetGet('validator');

            $validation = $validator->validate($data, [
                'login'                  => 'required',
                'password'                => 'required'
            ]);

            if ($validation->fails()) {
                $errors = $validation->errors();
                throw new AuthException(array_shift($errors->firstOfAll()));
            }

            Auth::authorize($this->em, $data);

            return new RedirectResponse('/');

        } catch (AuthException $exception) {
            Flash::error($exception->getMessage());
            return new RedirectResponse('/login');
        } catch (\Exception $exception) {
            Flash::error("Something went wrong");
            return new RedirectResponse('/login');
        }
    }

    /**
     * @return RedirectResponse
     */
    public function logout()
    {
        Auth::logout();

        return new RedirectResponse('/');
    }
}
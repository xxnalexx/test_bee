@extends('layouts.app')

@section('title','Home')

@section('content')

    <div class="starter-template">
        <h1>Bootstrap starter template</h1>
    </div>

    @include('tasks.form', [
        'action' => url('/tasks/store')
    ])

    <div class="tasks mt-3 mb-5">
        <div class="row mb-2">
            @foreach($columns as $key => $column)
                <div class="{{ isset($column['class']) ? $column['class'] : 'col' }}">
                    @if($column['sortable'])
                        <a href="{{ $sortTable->generateUrl($key) }}">{{ $column['title'] }}</a>
                    @else
                        {{ $column['title'] }}
                    @endif
                </div>
            @endforeach

            @if(\App\Kernel\Auth::check())
                <div class="col-1">
                    Actions
                </div>
            @endif
        </div>

        @if($tasks->getTotal() > 0)

            @foreach($tasks->getItems() as $task)
                <div class="row mb-2">
                    <div class="col-3">
                        {{ $task->getName() }}
                    </div>
                    <div class="col-3">
                        {{ $task->getEmail() }}
                    </div>
                    <div class="col-3">
                        {{ $task->getText() }}
                    </div>
                    <div class="col-2">
                        {{ \App\Repository\TaskRepository::getStatusText($task->getStatus()) }}
                    </div>

                    @if(\App\Kernel\Auth::check())
                        <div class="col-1">
                            <a href="{{ url('/tasks/' . $task->getId()) }}">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </div>
                    @endif
                </div>
            @endforeach

            @include('layouts.pagination', ['pagination' => $tasks])
        @else
            <div class="row mb-2">
                <div class="col-12 text-center">
                    No tasks yet
                </div>
            </div>
        @endif
    </div>
@endsection
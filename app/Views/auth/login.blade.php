@extends('layouts.app')

@section('title','Login')

@section('content')

<form method="POST" action="{{ url('/login') }}">
    <div class="form-group">
        <label for="exampleInputName">Login</label>
        <input type="text" name="login" class="form-control" id="exampleInputName" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPass">Password</label>
        <input type="password" name="password" class="form-control" id="exampleInputPass" required>
    </div>

    <button type="submit" class="btn btn-primary">Sing in</button>
</form>

@endsection

@if(isset($pagination) && $pagination->getTotal() > 0)
<nav aria-label="Page navigation example">
    <ul class="pagination">
        @for($i = 0; $i < $pagination->getPagesCount(); $i++)
            <li class="page-item"><a class="page-link" href="{{ $pagination->generateUrl($i+1) }}">{{ ($i+1) }}</a></li>
        @endfor
    </ul>
</nav>
@endif
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Todo List</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                @if(\App\Kernel\Auth::check())
                    <form action="{{ url('/logout') }}" method="POST">
                        <input type="submit" name="logout" value="Sign out">
                    </form>
                @else
                    <a class="nav-link" href="{{ url('/login') }}">Login <span class="sr-only">(current)</span></a>
                @endif
            </li>
        </ul>

    </div>
</nav>
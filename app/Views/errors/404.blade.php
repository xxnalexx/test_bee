@extends('layouts.app')

@section('title','404 Page not found')

@section('content')
    <div class="starter-template">
        <h1>404 Not found</h1>
    </div>
@endsection
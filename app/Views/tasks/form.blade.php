

<form method="POST" action="{{ $action }}">
    <div class="form-group">
        <label for="exampleInputName">Name</label>
        <input type="text" value="{{ isset($task) ? $task->getName() : '' }}" name="name" class="form-control" id="exampleInputName" required>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" name="email" value="{{ isset($task) ? $task->getEmail() : '' }}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Textarea</label>
        <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="3" required>{{ isset($task) ? $task->getText() : '' }}</textarea>
    </div>

    @if(\App\Kernel\Auth::check() && isset($task))
        <div class="form-group">
            <label for="exampleFormControlSelect">Status</label>
            <select class="form-control" name="status" id="exampleFormControlSelect">
                @foreach(\App\Repository\TaskRepository::getStatuses() as $key => $text)
                    <option
                            @if(isset($task) && $task->getStatus() == $key)
                            selected
                            @endif
                            value="{{ $key  }}">
                        {{ $text }}
                    </option>
                @endforeach
            </select>
        </div>
    @endif

    <button type="submit" class="btn btn-primary">{{ isset($task) ? 'Update' : 'Create' }}</button>
</form>
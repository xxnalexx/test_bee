@extends('layouts.app')

@section('title','Edit task')

@section('content')

    <div class="task-wrapper mb-5">
        <div class="starter-template">
            <h1>Editing task</h1>
        </div>

        <div class="row mt-2 mb-3">
            <div class="col">
                <a href="{{ url('/') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back to Tasks</a>
            </div>
        </div>

        @include('tasks.form', [
            'action' => url('/tasks/' .$task->getId()),
        ])
    </div>



@endsection
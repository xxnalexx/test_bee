<?php

namespace App\Helpers;

use App\Kernel\Url;
use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;

class SortTable
{

    public $sortParams = [];
    public $columns = [];
    /**
     * @var Container
     */
    public $container;

    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function getSortParams()
    {
        return $this->sortParams;
    }

    public function prepareSort($sort, array $columns)
    {
        $this->parseQuerySorting($sort, $columns);
    }

    /**
     * @param Request $request
     * @param array $columns
     * @return array
     */
    public function parseQuerySorting(?string $sort, array $columns)
    {
        if (!empty($sort)) {
            $params = explode("|", $sort);

            if (count($params) > 1
                &&
                \in_array(strtolower($params[0]), $columns)
                &&
                \in_array(strtolower($params[1]), ['asc', 'desc'])) {

                $this->sortParams = [
                    'column' => $params[0],
                    'dir' => $params[1],
                    'inverse' => $params[1] === 'asc' ? 'desc' : 'asc'
                ];
            }
        }
    }

    /**
     * @param array|null $sortParams
     * @return array|null
     */
    public function getSortOptions(?array $sortParams)
    {
        if (!empty($sortParams)) {
            return [$sortParams['column'] => $sortParams['dir']];
        }

        return $sortParams;
    }

    public function generateUrl($value, $param = 'sort')
    {
        $urlGenerator = $this->container->offsetGet(Url::class);

        $sort = $value . '|asc';

        if (!empty($this->sortParams)) {
            $sort = $value . '|' . $this->sortParams['inverse'];
        }

        $queryParams = $urlGenerator->replaceQueryParams($param, $sort);
        ;
        return $urlGenerator->fullUrl($urlGenerator->prepareQueryParams($queryParams));
    }
}
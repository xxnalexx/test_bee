<?php

namespace App\Helpers;

use App\Kernel\Url;
use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Tools\Pagination\Paginator;

class Pagination
{

    public $totalItems = 0;
    public $pagesCount = 1;
    public $currentPage = 1;
    public $items = [];

    /**
     * @var Container
     */
    public $container;

    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function paginate(Paginator $paginator, $currentPage, $pageSize)
    {
        $totalItems = count($paginator);
        $pagesCount = ceil($totalItems / $pageSize);

        $currentPage = $currentPage >= 1 && $currentPage <= $pagesCount ? $currentPage : 1;

        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($currentPage - 1))
            ->setMaxResults($pageSize);

        $this->currentPage = $currentPage;
        $this->totalItems = $totalItems;
        $this->items = $paginator;
        $this->pagesCount = $pagesCount;

        return $this;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function getTotal()
    {
        return $this->totalItems;
    }

    public function getPagesCount()
    {
        return $this->pagesCount;
    }

    public function generateUrl($page = 1)
    {
        $urlGenerator = $this->container->offsetGet(Url::class);
        $queryParams = $urlGenerator->replaceQueryParams('page', $page);
        ;
        return $urlGenerator->fullUrl($urlGenerator->prepareQueryParams($queryParams));
    }
}
<?php

namespace App\Providers;

use App\Helpers\Pagination;
use App\Helpers\SortTable;
use App\Kernel\NotFoundHandler;
use App\Kernel\RequestResponse;
use App\Kernel\Router;
use App\Kernel\Url;
use App\Middleware\AuthMiddleware;
use Philo\Blade\Blade;
use Pimple\Container;
use App\Kernel\CallableResolver;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use Rakit\Validation\Validator;

class DefaultServicesProvider
{
    /**
     * Register Slim's default services.
     *
     * @param Container $container A DI container
     */
    public function register($container)
    {
        if (!isset($container['request'])) {
            /**
             * PSR-7 Request object
             *
             * @param Container $container
             *
             * @return SymfonyRequest
             */
            $container['request'] = function ($container) {
                return SymfonyRequest::createFromGlobals();
            };
        }

        if (!isset($container['router'])) {
            /**
             *
             *
             * @param Container $container
             *
             * @return \App\Kernel\Router;
             */
            $container['router'] = function ($container) {
                $router = new Router;

                if (method_exists($router, 'setContainer')) {
                    $router->setContainer($container);
                }

                return $router;
            };
        }

        /**
         * Validator
         */
        if (!isset($container['validator'])) {
            /**
             *
             *
             * @param Container $container
             *
             * @return Validator;
             */
            $container['validator'] = function ($container) {
                return new Validator();
            };
        }

        /**
         * Blade views
         */
        if (!isset($container['view'])) {
            /**
             *
             *
             * @param Container $container
             *
             * @return \Philo\Blade\Blade;
             */
            $container['view'] = function () {
                $blade = new Blade(
                    APP_ROOT . '/app/Views',
                    APP_ROOT . '/cache/views'
                );

                return $blade;
            };
        }

        if (!isset($container['foundHandler'])) {
            $container['foundHandler'] = function () {
                return new RequestResponse;
            };
        }

        /**
         * Doctrine
         */
        if (!isset($container[EntityManager::class])) {
            $container[EntityManager::class] = function (Container $container): EntityManager {
                $config = Setup::createAnnotationMetadataConfiguration(
                    $container['app']['doctrine']['metadata_dirs'],
                    $container['app']['doctrine']['dev_mode']
                );

                $config->setMetadataDriverImpl(
                    new AnnotationDriver(
                        new AnnotationReader,
                        $container['app']['doctrine']['metadata_dirs']
                    )
                );

                $config->setMetadataCacheImpl(
                    new FilesystemCache(
                        $container['app']['doctrine']['cache_dir']
                    )
                );

                return EntityManager::create(
                    $container['app']['doctrine']['connection'],
                    $config
                );
            };
        }

        /**
         * Doctrine
         */
        if (!isset($container[Url::class])) {
            $container[Url::class] = function (Container $container) {
                $url = new Url;
                $url->setContainer($container);

                return $url;
            };
        }

        /**
         * Doctrine
         */
        if (!isset($container[Pagination::class])) {
            $container[Pagination::class] = function (Container $container) {
                $pagination = new Pagination;
                $pagination->setContainer($container);

                return $pagination;
            };
        }

        /**
         * Doctrine
         */
        if (!isset($container[SortTable::class])) {
            $container[SortTable::class] = function (Container $container) {
                $sortable = new SortTable;
                $sortable->setContainer($container);

                return $sortable;
            };
        }

        /**
         * Middleware
         */
        if (!isset($container[AuthMiddleware::class])) {
            $container[AuthMiddleware::class] = function (Container $container): AuthMiddleware {
                return new AuthMiddleware();
            };
        }

        if (!isset($container['notFoundHandler'])) {

            $container['notFoundHandler'] = function ($container) {
                $notFoundHandler = new NotFoundHandler;
                $notFoundHandler->setContainer($container);

                return $notFoundHandler;
            };
        }

        if (!isset($container['callableResolver'])) {
            /**
             * @param $container
             * @return CallableResolver
             */
            $container['callableResolver'] = function ($container) {
                return new CallableResolver($container);
            };
        }
    }
}
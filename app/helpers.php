<?php

if (!function_exists('url')) {
    function url($path) {
        if ($path) {
            return 'http://' . $_SERVER['HTTP_HOST'] . '' .$path;
        }

        return 'http://' . $_SERVER['HTTP_HOST'] . '/';
    }
}

if (!function_exists('asset')) {
    function asset($path)
    {
        return 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path;
    }
}

if (!function_exists('envVar')) {
    function envVar($key, $default = null)
    {
        return isset($_ENV[$key]) ? $_ENV[$key] : $default;
    }
}

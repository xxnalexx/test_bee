<?php

namespace App\Middleware;

use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Kernel\Auth;

class AuthMiddleware
{
    public function handle()
    {
        if (!Auth::check()) {
            return new RedirectResponse('/');
        }

        return true;
    }
}
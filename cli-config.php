<?php

$app = require  __DIR__.'/app/app.php';

$entityManager = $app->getContainer()->offsetGet(\Doctrine\ORM\EntityManager::class);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);